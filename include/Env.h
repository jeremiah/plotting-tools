#ifndef __ENV_H
#define __ENV_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <TStyle.h>
#include <TH1.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMarker.h>
#include <TH2.h>
#include <TPaveStats.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TEnv.h>
#include <TROOT.h>
#include <THashList.h>

#include <json.hpp>
#include "ReadFile.h"

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

class Env
{
public:
    Env(std::string /*i_name*/, TEnv* /*i_e*/);
    Env(std::string /*i_name*/, json& /*i_j*/);
    Env(std::string /*i_name*/, std::string /*i_f*/);
    ~Env();


    TEnv* getEnv(std::string i_name="");
    json getJson();

    void setData(TEnv* /*i_e*/);
    void setData(json& /*i_j*/);
    void setData(std::string /*i_f*/);

    void write(std::string i_name="");
    void writeFile(std::string i_name="");
private:
    Env(); // no default constructor
    void initialize(std::string /*i_name*/);

    TEnv* jsonToEnv(TEnv* /*i_e*/, json& /*i_j*/, std::string i_key=""); 
    json envToJson(TEnv* /*i_e*/);

    TEnv* m_e;
    json m_j;
    std::string m_f;

    std::string m_name;
    int m_format;

    bool m_run_e;
    bool m_run_j;
};

std::vector<std::string> split(std::string /*str*/, 
                               char /*del*/);

#endif //__ENV_H
