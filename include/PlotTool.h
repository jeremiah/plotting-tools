#ifndef __PLOTTOOL_H
#define __PLOTTOOL_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "TROOT.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <TPaveStats.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TMarker.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

class PlotTool
{
public:
    /// Deconstructor
    virtual ~PlotTool();

    /***
    Build & Fill histogram
    ***/
    void build();

    /***
    Write histogram
    ***/
    void write();
    void write(TH1* /*i_h*/);
    void write(TH2* /*i_h*/);

    /***
    Print histogram
    ***/
    void print(std::string /*i_dir*/,
               bool i_print=false,
               std::string i_ext="png");

    /***
    Set parameters from data
    * i_data: json data
    * i_h: TH1/TH2 data
    ***/
    virtual void setParameters(json& /*data*/);
    virtual void setParameters(TH1* /*i_h*/);
    virtual void setParameters(TH2* /*i_h*/);
    void setProjectionX(TH2* /*i_h*/);
    /***
    Get parameters of histogram into json data
    ***/
    json getParameters();

    /***
    Set histogram
    ***/
    virtual void setHisto(TH1* /*i_h*/, std::string /*i_n*/);
    virtual void setHisto(TH2* /*i_h*/, std::string /*i_n*/);
    void setHisto(TF1* /*i_f*/);

    /***
    Set data
    * setChip: chip name
    * setTitle: title of histogram
    * setData: data to fill into histogram
    ***/
    void setChip(std::string /*i_type*/,
                 std::string /*i_name*/,
                 std::string i_fe_name="");
    void setTitle(std::string /*i_t*/);
    void setData(json&       /*j*/,
                 std::string /*i_name*/,
                 std::string i_type="");
    void setData(TH1*        /*h*/,
                 std::string /*i_name*/,
                 std::string i_type="");
    void setData(TH2*        /*h*/,
                 std::string /*i_name*/,
                 std::string i_type="");

    /***
    Set dist/map data
    ***/
    void setDist(TH1* /*i_h*/);
    void setMap(TH2* /*i_h*/);

    /***
    Fit histogram
    ***/
    void fitHisto(TH1* /*i_h*/);

    /***
    Get TH1/TH2/TF1 data
    ***/
    TH1* getTH();
    TF1* getTF();

    void setThreshold(double /*thr*/);

    json setRms(TH1* /*i_h*/);
    json setRms();
    json setGaus(TF1* /*i_f*/);
    json setOcc(/*HistoOccupancy* */);
    json setNoi(/*HistoNoiseOccupancy* */);
    json setZero(int /*zero*/);
    json setZero();

    void setPercent(double per) { m_percent=per; };
protected:
    /// Constructor
    PlotTool ();

    /***
    Write/Build/Print histogram
    ***/
    virtual void writeHisto();
    virtual void buildHisto();
    virtual void printHisto(std::string /*i_dir*/,
                            bool        i_print=false,
                            std::string i_ext="png");
    /***
    Fill histogram from data
    ***/
    virtual void fillHisto(json& /*j*/) = 0;
    virtual void fillHisto(TH1* /*h*/) = 0;
    virtual void fillHisto(TH2* /*h*/) = 0;
    /***
    Draw histogram on the canvas
    ***/
    virtual void drawHisto() = 0;
    virtual void setCanvas() = 0;
    /***
    Set/Get text
    * setText0: 0 draw option
    * setText: Set information on the canvas
    * setTLatexLeft/Right: Set TLatex text on the left/right
    * Get TLatex text
    ***/
    void setText0(bool /*b*/);
    void setText();
    void setTLatexLeft(std::string /*i_text*/);
    void setTLatexRight(std::string /*i_text*/);
    std::vector <std::string> getTLatexLeft();
    std::vector <std::string> getTLatexRight();
    /***
    * whichSigma: get the deviation of the value
    * whichFE: get FE number from column number for RD53A AFE
    ***/
    int whichSigma(double /*i_value*/,
                   double /*i_mean*/,
                   double /*i_sigma*/,
                   double /*i_bin_width*/,
                   int    /*i_bin_num*/);
    int whichFE(int /*i_row*/,
                int /*i_col*/);
    /***
    Set style of histogram/canvas/latex
    ***/
    void style_TH1(TH1*        /*i_h*/,
                   std::string i_Xtitle="",
                   std::string i_Ytitle="",
                   int         i_color=1);
    void style_TGraph(TGraph*     /*i_h*/,
                      std::string i_Xtitle="",
                      std::string i_Ytitle="",
                      int         i_color=1);
    void style_THStack(THStack*    /*i_h*/,
                       std::string i_Xtitle="",
                       std::string i_Ytitle="");
    void style_TH2(TH2*        /*i_*/,
                   std::string i_Xtitle="",
                   std::string i_Ytitle="",
                   std::string i_Ztitle="",
                   int         i_color=kBird);
    void style_TF1(TF1* /*i_h*/,
                   int  i_color=632);
    void style_TH1Canvas(TCanvas* /*i_c*/);
    void style_TH2Canvas(TCanvas* /*i_c*/);
    void style_THStackCanvas(TCanvas* /*i_c*/);
    void style_TLatex(TLatex* /*i_latex*/,
                      int     /*i_align*/,
                      int     /*i_font*/,
                      int     /*i_size*/,
                      int     i_color=922);
    /***
    get FE number from FE name for RD53A AFE
    ***/
    int getFE(std::string /*n*/);
    int getFE();

    std::vector <float> thresholdPercent(double /*gaussMean*/);

    /// variables
    TH1* m_h;
    json m_j;
    TH1* m_h1;
    TH2* m_h2;
    TH1* m_dist;
    TH2* m_map;
    TF1* m_f;
    TCanvas* m_c;

    std::string m_fe_name;
    std::string m_type;
    std::string m_histo_name;
    std::string m_histo_type;
    std::string m_data_name;
    std::string m_data_type;
    std::string m_chip_type;
    std::string m_chip_name;
    std::string m_title;

    int m_data_format;
    double m_thr;
    int m_zeros;
    int m_occs;
    int m_nois;

    std::vector <std::string> m_latex_left;
    std::vector <std::string> m_latex_right;
    std::vector <double> m_latex_pos;
    bool m_text0;
    typedef struct {
        typedef struct {
            int nbins;
            double low;
            double high;
            std::string title;
            std::vector <std::string> label;
        } Axis;
        typedef struct {
            Axis x;
            Axis y;
            Axis z;
            std::string type;
        } Histo;
        Histo histo;
        Histo data;
        bool overwrite;
    } PlotAxis;
    PlotAxis m_axis;

    double m_percent;
    bool m_gaus;

    std::vector <int> m_color = {597, 806, 824, 865}; //(kOrange+6, kSpring+4, kAzure+5, kBlue-3)
private:
};

bool sortbysec(const std::pair<double, float> &/*a*/, const std::pair<double, float> &/*b*/);

#endif //__PLOTTOOL_H

