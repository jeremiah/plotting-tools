################################
# Author: Arisa Kubota
# Email: arisa.kubota at cern.ch
# Date : Oct. 2019
################################

# Simple Makefile for multiple executeables + X other files/classes
# Main files .cxx, Classes in .cpp and .h
# Define INCLUDE, LIBS and EXE accordingly

MAKEFLAGS=--warn-undefined-variables
MKDIR := mkdir -p

INCLUDE = -I./. $(shell root-config --cflags) -I./include
LIBS = $(shell root-config --libs)

# Define compiler and flags
CXX = g++
#CFLAGS = -g -O0 -Wall $(INCLUDE) -DDEBUG
CFLAGS = -g -O0 -Wall $(INCLUDE)
LDFLAGS = $(LIBS) -Wl,-rpath,$(shell root-config --libdir)

SSRCDIR = ./src
OBJDIR = ./build
BINDIR = ./bin
SRCDIR = ./tools

$(shell $(MKDIR) $(SRCDIR) $(OBJDIR) $(BINDIR))

SSRC = $(wildcard ${SSRCDIR}/*.cxx)
OOBJ = $(addprefix $(OBJDIR)/, $(notdir $(SSRC:.cxx=.o)))
SRC = $(wildcard $(SRCDIR)/*.cxx)
OBJ = $(addprefix $(OBJDIR)/, $(notdir $(SRC:.cxx=.o)))
EXE = $(addprefix $(BINDIR)/, $(notdir $(SRC:.cxx=)))

all: $(OOBJ) $(EXE) $(OBJ)
$(OBJDIR)/%.o: $(SSRCDIR)/%.cxx
	@$(CXX) -c $(CFLAGS) $< -o $@
	@echo "[Compiling] $@"

$(OBJDIR)/%.o: $(SRCDIR)/%.cxx
	@$(CXX) -c $(CFLAGS) $< -o $@
	@echo "[Compiling] $@"

$(BINDIR)/%: $(OBJDIR)/%.o $(OOBJ)
	@$(CXX) $(CFLAGS) $(OOBJ) $< $(LDFLAGS) -o $@
	@echo "[Linking] $@"

.PHONY: clean
clean:
	rm -f $(OOBJ)
	rm -f $(OBJ)
	rm -f $(EXE)
