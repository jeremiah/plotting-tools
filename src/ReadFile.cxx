#include "ReadFile.h"

json datToJson(std::string i_path) {
#ifdef DEBUG
    std::cout << "datToJson(" << i_path << ")" << std::endl;
#endif
    json j;
    j["is_null"] = true;
    std::ifstream file_ifs(i_path);

    std::string type;
    std::string name;
    std::string xaxistitle, yaxistitle, zaxistitle;

    int nbinsx, nbinsy;
    double xlow, ylow;
    double xhigh, yhigh;
    int underflow, overflow;

    std::getline(file_ifs, type);
    std::getline(file_ifs, name);
    std::getline(file_ifs, xaxistitle);
    std::getline(file_ifs, yaxistitle);
    std::getline(file_ifs, zaxistitle);

    type = type.substr(0,7); // EOL char kept by getline()

    if (type!="Histo1d"&&type!="Histo2d"&&type!="Histo3d") {
        std::cerr << "[ERROR] Something wrong with file: " << i_path << std::endl;
        return j;
    }
    j["Type"] = type;
    j["Name"] = name;
    j["x"]["AxisTitle"] = xaxistitle;
    j["y"]["AxisTitle"] = yaxistitle;
    j["z"]["AxisTitle"] = zaxistitle;

    file_ifs >> nbinsx >> xlow >> xhigh;
    j["x"]["Bins"] = nbinsx;
    j["x"]["Low"]  = xlow;
    j["x"]["High"] = xhigh;

    if (type == "Histo2d") {
        file_ifs >> nbinsy >> ylow >> yhigh;
        j["y"]["Bins"] = nbinsy;
        j["y"]["Low"]  = ylow;
        j["y"]["High"] = yhigh;
    } else {
        nbinsy = 1;
        j["y"]["Bins"] = nbinsy;
        j["y"]["Low"]  = 0;
        j["y"]["High"] = 1;
    }

    file_ifs >> underflow >> overflow;
    j["Underflow"] = underflow;
    j["Overflow"] = overflow;

    if (!file_ifs) {
        std::cerr << "[ERROR] Something wrong with file: " << i_path << std::endl;
        return j;
    }

    //json data;
    double tmp;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            file_ifs>>tmp;
            if (!file_ifs) {
                std::cerr << "[ERROR] Something wrong with file: " << i_path << std::endl;
                return j;
            }
            if (type == "Histo2d") {
                j["Data"][col][row] = tmp;
            } else {
                j["Data"][col] = tmp;
            }
        }
    }
    j["is_null"] = false;

    return j;
}

json readScanLog(json& i_j) {
#ifdef DEBUG
    std::cout << "readScanLog(json)" << std::endl;
#endif
    json log_json = i_j;
    json chip_json;
    std::string chip_type;
    if (log_json["connectivity"].type()==json::value_t::array) {
        for (auto conn_cfg : log_json["connectivity"]) { // check connectivity
            chip_type = conn_cfg["chipType"];
            for (auto chip : conn_cfg["chips"]) {
                chip_json.push_back(chip);
            }
        }
    } else {
        chip_type = log_json["connectivity"]["chipType"];
        chip_json = log_json["connectivity"]["chips"];
    }

    json j;
    if (chip_type=="FEI4B") chip_type = "FE-I4B";
    j["chipType"] = chip_type;
    j["chips"] = json::array();
    for (auto chip : chip_json) {
        std::string cfg_path = chip["config"];
        if (cfg_path.find("/")!=std::string::npos) cfg_path = cfg_path.substr(cfg_path.find_last_of("/")+1);
        chip["config"] = cfg_path;
        j["chips"].push_back(chip);
    }

    std::string key[3] = { "testType", "targetCharge", "targetTot" };
    for (int i=0; i<3; i++) {
        if (!log_json[key[i]].empty()) {
            j[key[i]] = log_json[key[i]];
        }
    }
    return j;
}

json readChipGlobCfg(json& i_j) {
#ifdef DEBUG
    std::cout << "readChipGlobCfg(json)" << std::endl;
#endif
    json file_json = i_j;
    for (auto& j : file_json.items()) {
        if (file_json[j.key()].is_object()) file_json[j.key()].erase("PixelConfig");
    }
    return file_json;
}

json fileToJson(std::string i_path, std::string i_type) {
#ifdef DEBUG
    std::cout << "fileToJson(" << i_path << ", " << i_type << ")" << std::endl;
#endif
    json j = {{ "is_null", true }};
    if (!checkFile(i_path, "error")) return j;
    std::ifstream file_ifs(i_path);
    try {
        j = json::parse(file_ifs);
    } catch (json::parse_error &e) {
        std::cerr << "[ERROR] Could not parse the file: " << i_path << std::endl;
        std::cerr << "        what(): " << e.what() << std::endl;
        j["is_null"] = true;
        return j;
    }

    json jout;
    if (i_type=="scanLog") {
        jout = readScanLog(j);
    } else if (i_type=="scanCfg") {
        jout = j;
    } else if (i_type=="plotCfg") {
        jout = j;
    } else if (i_type=="beforeCfg"||i_type=="afterCfg") {
        jout = readChipGlobCfg(j);
    } else {
        jout = j;
    }

    jout["is_null"] = false;
    return jout;
}

json readChipPixCfg(std::string i_path, std::string i_type) {
#ifdef DEBUG
    std::cout << "readChipPixCfg(" << i_path << ", " << i_type << ")" << std::endl;
#endif
    json j = {{ "is_null", true }};
    if (!checkFile(i_path)) return j;

    std::ifstream file_ifs(i_path);
    try {
        j = json::parse(file_ifs);
    } catch (json::parse_error &e) {
        std::cerr << "[ERROR] Could not parse the file: " << i_path << std::endl;
        std::cerr << "        what(): " << e.what() << std::endl;
        j = {{ "is_null", true }};
        return j;
    }

    std::string chip_name = "JohnDoe";
    if      (i_type=="FE-I4B")  chip_name = j[i_type]["name"];
    else if (i_type=="RD53A")   chip_name = j[i_type]["Parameter"]["Name"];
    else if (i_type=="RD53B")   chip_name = j[i_type]["Parameter"]["Name"];
    else if (i_type=="FE65-P2") chip_name = j[i_type]["name"];
    else if (i_type=="Star")    chip_name = j["name"];

    json chipCfg;
    if (!j[i_type]["PixelConfig"].is_null()) {
        json pixCfg;
        bool lineRow;
        int line=0;
        int colnum, rownum;
        for (auto& i: j[i_type]["PixelConfig"]) {
            if (i.count("Row")==1||i.count("row")==1) lineRow = true;
            else if (i.count("Col")==1||i.count("col")==1) lineRow = false;

            for (auto& el: i.items()) {
                if (el.key()=="Row"||el.key()=="row"||el.key()=="Col"||el.key()=="col") {
                    continue;
                } else if (pixCfg.count(el.key())==0) {
                    json cfg;
                    cfg["Type"] = "Histo2d";
                    cfg["Name"] = el.key();
                    cfg["x"]["AxisTitle"] = "Column";
                    cfg["y"]["AxisTitle"] = "Row";
                    cfg["z"]["AxisTitle"] = el.key();
                    pixCfg[el.key()] = cfg;
                } else if (pixCfg[el.key()]["x"].count("Bins")==0&&pixCfg[el.key()]["y"].count("Bins")==0) {
                    pixCfg[el.key()]["x"]["Bins"] = colnum;
                    pixCfg[el.key()]["x"]["Low"]  = 0.5;
                    pixCfg[el.key()]["x"]["High"] = colnum+0.5;
                    pixCfg[el.key()]["y"]["Bins"] = rownum;
                    pixCfg[el.key()]["y"]["Low"]  = 0.5;
                    pixCfg[el.key()]["y"]["High"] = rownum+0.5;
                    pixCfg[el.key()]["Underflow"] = 0;
                    pixCfg[el.key()]["Overflow"] = 0;
                }
                if (lineRow) {
                    int col = 0;
                    for (auto tmp: el.value()) {
                        pixCfg[el.key()]["Data"][col][line] = tmp;
                        col++;
                    }
                    colnum = col;
                } else {
                    int col = line;
                    int row = 0;
                    for (auto tmp: el.value()) {
                        pixCfg[el.key()]["Data"][col][row] = tmp;
                        row++;
                    }
                    rownum = row;
                }
            }
            line++;
            if (lineRow) rownum = j[i_type]["PixelConfig"].size();
            else colnum = j[i_type]["PixelConfig"].size();
        }
        chipCfg["PixelConfig"] = pixCfg;
    }

    chipCfg["Type"] = i_type;
    chipCfg["Name"] = chip_name;
    chipCfg["is_null"] = false;

    return chipCfg;
}

json readDataFile(std::string i_path, std::string i_name) {
#ifdef DEBUG
    std::cout << "readDataFile : " << i_path << " chipName : " << i_name << std::endl;
#endif

    json data_json = {{ "is_null", true }};

    if (!checkFile(i_path)) return data_json;
    if (i_path.find(i_name.c_str())==std::string::npos) return data_json; // skip if file doesn't have chip name
    if (
        i_path.find_last_of(".")==std::string::npos &&
        i_path.substr(i_path.find_last_of("."))!=".dat" &&
        i_path.substr(i_path.find_last_of("."))!=".json"
    ) return data_json; // skip if file is not data file


    if (i_path.substr(i_path.find_last_of("."))==".dat") {
        data_json = datToJson(i_path);
    } else if (i_path.substr(i_path.find_last_of("."))==".json") {
        data_json = fileToJson(i_path);
    }

    if (data_json["Type"].empty()||data_json["Name"].empty()) {
        data_json["is_null"] = true;
        return data_json;
    }

    data_json["is_null"] = false;

    return data_json;
}

/// Check file
bool checkFile(std::string i_path, std::string i_level) {
#ifdef DEBUG
    std::cout << "checkFile(" << i_path << ", " << i_level << ")" << std::endl;
#endif
    struct stat filestat;
    bool file_exist = true;
    if (stat(i_path.c_str(), &filestat)) { // skip if file is invalid
        file_exist = false;
        if (i_level=="warning") std::cout << "[WARNING] Not found the file at " << i_path << std::endl;
        if (i_level=="error") {
            std::cout << "[ERROR] Not found the file at " << i_path << std::endl;
        }
    } else if (S_ISDIR(filestat.st_mode)) { // skip if file is a directory
        file_exist = false;
        if (i_level=="warning") std::cout << "[WARNING] " << i_path << " is directory, not file." << std::endl;
        if (i_level=="error") {
            std::cout << "[ERROR] " << i_path << " is directory, not file." << std::endl;
        }
    }
    return file_exist;
}

