#include "HistoProjection.h"

//////////////
/// Public ///
//////////////
//==============================
HistoProjection::HistoProjection():
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoProjection::HistoProjection()" << std::endl;
#endif
};
HistoProjection::~HistoProjection(){
#ifdef DEBUG
    std::cout << "HistoProjection::~HistoProjection()" << std::endl;
#endif
};
//==============================
///////////////
/// Private ///
///////////////
//==============================
void HistoProjection::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoProjection::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    std::string overflow_str = Form("Overflow = %.0f", m_h->GetBinContent(m_h->GetNbinsX()+1));
    this->setTLatexLeft(overflow_str);
    std::string underflow_str = Form("Underflow = %.0f", m_h->GetBinContent(0));
    this->setTLatexLeft(underflow_str);

    m_c->cd();
    //if (m_axis.overwrite) {
    //    double mean = m_h->GetMean();
    //    double rms = m_h->GetRMS();
    //    double min = (mean-5*rms<0)?-0.5:(mean-5*rms);
    //    double max = (mean+5*rms);
    //    m_h->GetXaxis()->SetRangeUser(min, max);
    //}
    //double mean = m_h->GetMean();
    //double rms = m_h->GetRMS();
    //double min = (mean-5*rms<0)?-0.5:(mean-5*rms);
    //double max = (mean+5*rms);
    //m_h->GetXaxis()->SetRangeUser(min, max);

    m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.5));
    m_c->Modified();
    m_c->Update();
};
void HistoProjection::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "HistoProjection::fillHisto(json)" << std::endl;
#endif
    int nbinsx = i_j["x"]["Bins"];
    int nbinsy = i_j["y"]["Bins"];
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_j["Data"][col][row];
            m_h->Fill(tmp);
            if (tmp==0) m_zeros++;
        }
    }
};
void HistoProjection::fillHisto(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoProjection::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = i_h->GetNbinsX();
    int nbinsy = i_h->GetNbinsY();
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_h->GetBinContent(col+1, row+1);
            m_h->Fill(tmp);
            if (tmp==0) m_zeros++;
        }
    }
};
