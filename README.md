## Plotting Tool

### Commands

* plotFromDir
    * Make one ROOT file including plot/config data from result directory
    * Make verious plots from result directory
* plotFromRoot
    * Make one ROOT file including plot/config data from result ROOT file
    * Make verious plots from result ROOT file
* getJsonData
    * Get specific JSON data from result ROOT file

### Quick Tutorial

#### 0. Git Clone

```bash
$ git clone <this project>
```

#### 1. Preparation

* Make Compile

```bash
$ cd plotting-tool
$ make
```

* Make a path to ROOT liblaries

```bash
$ source <path/to/ROOT/install/dir>/bin/thisroot.sh
```

#### 2. Make ROOT file/plots from result directory by YARR

```bash
$ ./bin/plotFromDir -i <path/to/result/directory>
e.g.) $ ./bin/plotFromDir -i ./data/last_scan
<some texts>
Done.
### output ROOT file to ./data/last_scan/rootfile.root
### output plots to ./data/last_scan with option '-P'
```

In ROOT file, there are result data as TH1 or TH2 class, pixel config data as TH2 class, and config data as TEnv class.

#### 3. Get JSON data from ROOT file

```bash
$ ./bin/getJsonData -i ./data/last_scan/rootfile.root

TEnv List:
	scanLog
	scanCfg
	dataLog

Enter the name of TEnv Object to get.
> scanCfg

<some texts>

Output scanCfg in ./data/last_scan/scanCfg_root.json
```

#### 4. Get Plots from ROOT file

```bash
$ ./bin/plotFromRoot -i ./data/last_scan/rootfile.root -P

<some texts>

Done.
### output plots to ./data/last_scan/ with option '-P'
```

### Plots

#### Standard plots

(e.g.) analogscan

|L1Dist(TH1)|EnMask(TH2)|OccupancyMap(TH2)
|---|---|---|
|![l1dist_root](images/L1Dist_root.png)|![enmask_root](images/EnMask_root.png)|![occupancy_root](images/OccupancyMap_root.png)|

#### OccupancyDistribution (OccupancyMap)

(e.g.) OccupancyMap (analogscan)

|Standard Map|Projection Distribution|Occupancy Distribution|
|---|---|---|
|![occupancy_root](images/OccupancyMap_root.png)|![occupancy_projection_root](images/OccupancyMap_projection_root.png)|![occupancy_occupancy_root](images/OccupancyMap_occupancy_root.png)|

#### RMS Deviation (MeanTotMap, SigmaTotMap, ThresholdMap, NoiseMap)

(e.g.) MeanTotMap (totscan)

|Standard Map|Projection Distribution|RMS Deviation|
|---|---|---|
|![tot_root](images/MeanTotMap_0_root.png)|![tot_projection_root](images/MeanTotMap_0_projection_root.png)|![tot_rms_root](images/MeanTotMap_0_rms_root.png)|

#### Gausian Deviation (ThresholdMap, NoiseMap)

(e.g.) ThresholdMap (thresholdscan)

|Standard Map|Projection Distribution|RMS Deviation|Gaussian Deviation|
|---|---|---|---|
|![threshold_root](images/ThresholdMap_0_root.png)|![threshold_projection_root](images/ThresholdMap_0_projection_root.png)|![threshold_rms_root](images/ThresholdMap_0_rms_root.png)|![threshold_gaussian_root](images/ThresholdMap_0_gaus_root.png)|

#### Noise Occupancy (NoiseOccupancy)

(e.g.) NoiseOccupancy (noisescan)

|Standard Map|Projection Distribution|Noise Occupancy Distribution|
|---|---|---|
|![noiseoccupancy_root](images/NoiseOccupancy_root.png)|![noiseoccupancy_projection_root](images/NoiseOccupancy_projection_root.png)|![noiseoccupancy_noiseoccupancy_root](images/NoiseOccupancy_noiseoccupancy_root.png)|

#### Pixel config

(e.g.) TDAC

|Standard Map|Projection Distribution|
|---|---|
|![tdac_root](images/TDAC_root.png)|![tdac_projection_root](images/TDAC_projection_root.png)|

### Optional Arguments

#### plotFromDir

```bash
$ ./bin/plotFromDir -h

Help:
 -h/-H     : Shows this.
 -P        : Set the print mode True.
 -i <dir>  : Result directory.
 -o <dir>  : Output directory. (Default. path/to/input/dir)
 -e <ext>  : Extension. (Default. png)
 -p <json> : Parameter config file.
```

#### plotFromRoot

```bash
$ ./bin/plotFromRoot -h

Help:
 -h/-H     : Shows this.
 -P        : Set the print mode True.
 -i <file> : Input ROOT file name with option '-r'.
 -o <dir>  : Output directory. (Default. path/to/input/dir)
 -e <ext>  : Extension. (Default. png)
 -p <json> : Parameter config file.
```

#### getJsonData

```bash
$ ./bin/getJsonData -h

Help:
 -h/-H: Shows this.
 -i <file> : Input ROOT file name.
 -o <file> : Output file name. (Default. path/to/input/dir/output.json)
 -k <key>  : Config file key if specified.
```
